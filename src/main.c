#include <HGE/HGE_Core.h>
#include "map.h"
#include "pond.h"
#include "player.h"
#include "game_manager.h"
#include "dialogue.h"
#include "camera_fx.h"
//#include "particles.h"

int main() {
  hge_window window = { "Friends Jam I", 768, 768 };
  hgeInit(60, window, HGE_INIT_ECS | HGE_INIT_RENDERING);

  hgeResourcesLoadTexture("res/monochrome_packed.png", "onebit");

  //hgeAddBaseSystems();
  hgeAddSystem(CameraSystem, 3, "Camera", "Position", "Orientation");
  //hgeAddSystem(FreeCam, 3, "Camera", "FreeMove", "Position");

  // Game Systems
  hgeAddSystem(System_CameraFX, 2, "Position", "camera_fx");
  hgeAddSystem(System_End, 2, "pond", "end");
  hgeAddSystem(System_Slider, 2, "pond", "slider");
  hgeAddSystem(System_Player, 2, "pond", "playable");
  hgeAddSystem(System_Pond, 1, "pond");
  hgeAddSystem(System_PondRendering, 1, "pond");
  hgeAddSystem(System_Map, 1, "map");
  hgeAddSystem(System_Dialogue, 1, "dialogue");
  //hgeAddSystem(System_Particles, 1, "particle_system");

  // Camera
  hge_entity* camera_entity = hgeCreateEntity();
	hge_camera cam = {true, true, 1.f/3.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth() / (float)hgeWindowHeight(), -100.0f, 100.0f };
	hge_vec3 camera_position = { (MAP_WIDTH-1)/2.f * 16, (-MAP_HEIGHT+1)/2.f * 16, 0};
	orientation_component camera_orientation = {0.0f, -90.0f, 0.0f};
	hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
	tag_component activecam_tag;
	hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));
	camera_fx camera_fx_component;
  camera_fx_component.desired_position.x = 0;
  camera_fx_component.desired_position.y = 0;
  camera_fx_component.shake_vector.x = 0;
  camera_fx_component.shake_vector.y = 0;
	hgeAddComponent(camera_entity, hgeCreateComponent("camera_fx", &camera_fx_component, sizeof(camera_fx_component)));


  hge_entity* gui_entity = hgeCreateEntity();
  dialogue c_dialogue;
  hgeAddComponent(gui_entity, hgeCreateComponent("dialogue", &c_dialogue, sizeof(c_dialogue)));

  LoadLevel("res/levels/tutorial1.tmx");

  hgeStart();
  return 0;
}
