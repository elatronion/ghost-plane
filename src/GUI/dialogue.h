#ifndef DIALOGUE_H
#define DIALOGUE_H
#include <HGE/HGE_Core.h>

typedef struct {
  // ...
} dialogue;

void System_Dialogue(hge_entity* entity, dialogue* d);

#endif
