#include "game_manager.h"
#include "player.h"
#include "camera_fx.h"
#include "tmx.h"

bool is_replay = false;

void SetReplay(bool b) { is_replay = b; }
bool GetReplay() { return is_replay; }

game_state state = main_menu;
char last_loaded_scene[255];

hge_entity* game_entities[1000];
int num_game_entities = 0;

void AddEntityToGameEntities(hge_entity* entity) {
  game_entities[num_game_entities] = entity;
  num_game_entities++;
}

void DestroyAllGameEntities() {
  for(int i = 0; i < num_game_entities; i++)
    hgeDestroyEntity(game_entities[i]);
  num_game_entities = 0;
}

void UpdateState() {
  // camera_fx
  camera_fx* requested_camfx;
  hge_ecs_request camfx_request = hgeECSRequest(1, "camera_fx");
  for(int i = 0; i < camfx_request.NUM_ENTITIES; i++) {
    hge_entity* camera_entity = camfx_request.entities[i];
    requested_camfx = camera_entity->components[hgeQuery(camera_entity, "camera_fx")].data;
  }

  switch(state) {
    case main_menu:
    break;
    case game_play:
    break;
    case game_win:
      SetReplay(false);
      requested_camfx->shake_intensity = 25.f;
    break;
    case game_over:
      SetReplay(false);
      requested_camfx->shake_intensity = 50.f;
    break;
  }
}

#define GHOST_GID_UP 983 + 1
#define GHOST_GID_RIGHT 984 + 1
#define GHOST_GID_DOWN 985 + 1
#define GHOST_GID_LEFT 986 + 1

void ParseTMXData(tmx_map* map) {
  printf("Map Size: %dx%d\n", map->width, map->height);

  tmx_layer* layer = map->ly_head;
  while(layer != NULL) {
    if(strcmp(layer->name, "walls") == 0) {
      game_map* g_map = AddMap();
      for(int i = 0; i < map->width*map->height; i++) {
        int x = i%map->width;
        int y = i/map->width;
        // Tiled adds one to account for tiles that are null '0'
        g_map->tiles[x][y] = layer->content.gids[i]-1;
        if(g_map->tiles[x][y] < 0) g_map->tiles[x][y] = 0;
      }
    } else if(strcmp(layer->name, "ghosts") == 0) {
      printf("Doing layer '%s'\n", layer->name);
      v2i direction_ghost = { 0, 0 };
      hge_vec2 sprite_ghost = { 26, 6 };
      hge_vec3 color_ghost = { 1, 0, 0 };
      for(int i = 0; i < map->width*map->height; i++) {
        v2i position_ghost = { i%map->width, i/map->width };
        if(layer->content.gids[i] <= 0) continue;
        switch(layer->content.gids[i]) {
          case GHOST_GID_UP:
          direction_ghost.x = 0;
          direction_ghost.y = -1;
          break;
          case GHOST_GID_RIGHT:
          direction_ghost.x = 1;
          direction_ghost.y = 0;
          break;
          case GHOST_GID_DOWN:
          direction_ghost.x = 0;
          direction_ghost.y = 1;
          break;
          case GHOST_GID_LEFT:
          direction_ghost.x = -1;
          direction_ghost.y = 0;
          break;
        }
        AddSlider(position_ghost, direction_ghost, sprite_ghost, color_ghost);
      }
    } else if(strcmp(layer->name, "player") == 0) {
      for(int i = 0; i < map->width*map->height; i++) {
        int x = i%map->width;
        int y = i/map->width;
        if(layer->content.gids[i] > 0) {
          v2i position = { x, y };
          AddPlayer(position);
          break;
        }
      }
    } else if(strcmp(layer->name, "end") == 0) {
      char next_level[255] = "res/levels/error.tmx";
      tmx_property* property_next_level = tmx_get_property(layer->properties, "next_level");
      if(property_next_level)
        strcpy(next_level, property_next_level->value.string);
      for(int i = 0; i < map->width*map->height; i++) {
        int x = i%map->width;
        int y = i/map->width;
        if(layer->content.gids[i] > 0) {
          v2i position = { x, y };
          AddEnd(position, next_level);
          //break;
        }
      }
    }
    layer = layer->next;
  }
}

void LoadLevel(const char* level_path) {
  printf("Loading Level '%s'\n", level_path);
  strcpy(last_loaded_scene, level_path);
  DestroyAllGameEntities();

  tmx_map *map = tmx_load(last_loaded_scene);
  if (!map) {
		tmx_perror("Cannot load map");
		return;
	}
  ParseTMXData(map);
  tmx_map_free(map);

  SetState(game_start);
}

void ReloadLevel() {
  DestroyAllGameEntities();
  LoadLevel(last_loaded_scene);
}

game_map* AddMap() {
  hge_entity* map_entity = hgeCreateEntity();
  game_map map;
  for(int y = 0; y < MAP_HEIGHT; y++)
  for(int x = 0; x < MAP_WIDTH; x++) {
    map.tiles[x][y] = TILE_FLOOR;
  }
  hge_component map_c = hgeCreateComponent("map", &map, sizeof(map));
  hgeAddComponent(map_entity, map_c);
  AddEntityToGameEntities(map_entity);
  return map_c.data;
}

void AddEnd(v2i position, const char* next_level) {
  hge_entity* end_entity = hgeCreateEntity();
  pond end_pond;
  end_pond.position = position;
  end_pond.direction.x = 0;
  end_pond.direction.y = 0;
  end_pond.flags = NULL;
  end_pond.sprite.x = 31;
  end_pond.sprite.y = 11;
  end_pond.color.x = 0.466666667f;
  end_pond.color.y = 0.356862745f;
  end_pond.color.z = 0.639215686f;
  end_pond.ghost = false;
  end_pond.collision_flags = POND_COLLISION_FLAG_NONE;
  end_pond.update = false;
  hgeAddComponent(end_entity, hgeCreateComponent("pond", &end_pond, sizeof(end_pond)));
  end end_c;
  //end_c.next_level = next_level;
  strcpy(end_c.next_level, next_level);
  hgeAddComponent(end_entity, hgeCreateComponent("end", &end_c, sizeof(end_c)));
  AddEntityToGameEntities(end_entity);
}

#include "particles.h"
void AddPlayer(v2i position) {
  hge_entity* player_entity = hgeCreateEntity();
  pond player_pond;
  player_pond.position = position;
  player_pond.direction.x = 0;
  player_pond.direction.y = 0;
  player_pond.flags = NULL;
  player_pond.sprite.x = 24;
  player_pond.sprite.y = 2;
  player_pond.color.x = 0.0f;
  player_pond.color.y = 1.0f;
  player_pond.color.z = 0.0f;
  player_pond.ghost = false;
  player_pond.collision_flags = POND_COLLISION_FLAG_NONE;
  player_pond.update = true;
  hgeAddComponent(player_entity, hgeCreateComponent("pond", &player_pond, sizeof(player_pond)));
  player player_c;
  player_c.observe_tokens = 5;
  player_c.first_loop = true;
  hgeAddComponent(player_entity, hgeCreateComponent("playable", &player_c, sizeof(player_c)));
  AddEntityToGameEntities(player_entity);
}

void AddSlider(v2i position, v2i direction, hge_vec2 sprite, hge_vec3 color) {
  hge_entity* slider_entity = hgeCreateEntity();
  pond slider_pond;
  slider_pond.position = position;
  slider_pond.direction = direction;
  slider_pond.flags = NULL;
  slider_pond.sprite = sprite;
  slider_pond.color = color;
  slider_pond.ghost = true;
  slider_pond.collision_flags = POND_COLLISION_FLAG_NONE;
  slider_pond.update = false;
  hgeAddComponent(slider_entity, hgeCreateComponent("pond", &slider_pond, sizeof(slider_pond)));
  slider c_slider;
  hgeAddComponent(slider_entity, hgeCreateComponent("slider", &c_slider, sizeof(c_slider)));
  AddEntityToGameEntities(slider_entity);
}

void SetState(game_state s) { state = s; UpdateState(); }
game_state GetState() { return state; }
