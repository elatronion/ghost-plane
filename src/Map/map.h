#ifndef GAME_MAP_H
#define GAME_MAP_H
#include <HGE/HGE_Core.h>
#include "jamutils.h"

#define SPRITE_SHEET_WIDTH  47
#define SPRITE_SHEET_HEIGHT 21

#define MAP_WIDTH 16
#define MAP_HEIGHT 16

#define TILE_FLOOR  0
#define TILE_WALL   1

typedef struct {
  int tiles[MAP_WIDTH][MAP_HEIGHT];
} game_map;

void System_Map(hge_entity* entity, game_map* map);

#endif
