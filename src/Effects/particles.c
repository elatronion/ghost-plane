#include "particles.h"

void AddParticle(particle_system* particle_sys) {
  if(particle_sys->num_particles > MAX_PARTICLES-2) return;
  particle p;
  p.position.x = 0;
  p.position.y = 0;

  p.velocity.x = 0;
  p.velocity.y = -50;
  particle_sys->particles[particle_sys->num_particles] = p;
  particle_sys->num_particles++;
}

void System_Particles(hge_entity* entity, particle_system* particle_sys) {
  AddParticle(particle_sys);

  hge_vec2 frame_resolution = { SPRITE_WIDTH, SPRITE_HEIGHT };
  hge_vec3 scale = { SPRITE_WIDTH, SPRITE_HEIGHT, 0 };
  //hge_vec3 position = { 0 * scale.x, -0 * scale.y, 99 };
  hge_vec2 sprite = { 27, 21 };
  for(int i = 0; i < particle_sys->num_particles; i++) {
    // Physics
    particle_sys->particles[i].position.x += particle_sys->particles[i].velocity.x * hgeDeltaTime();
    particle_sys->particles[i].position.y += particle_sys->particles[i].velocity.y * hgeDeltaTime();

    // Rendering
    hge_vec3 position = { particle_sys->particles[i].position.x * scale.x, -particle_sys->particles[i].position.y * scale.y, 99 };
    hgeRenderSpriteSheet(
      hgeResourcesQueryShader("sprite_shader"),
      hgeResourcesQueryTexture("onebit"),
      position, scale, 0.0f,
      frame_resolution, sprite);
  }
}
