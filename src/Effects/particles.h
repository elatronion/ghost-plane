#ifndef PARTICLES_H
#define PARTICLES_H
#include <HGE/HGE_Core.h>
#include "jamutils.h"

#define MAX_PARTICLES 500

typedef struct {
  v2i position;
  hge_vec2 velocity;
} particle;

typedef struct {
  particle particles[MAX_PARTICLES];
  int num_particles;
} particle_system;

void System_Particles(hge_entity* entity, particle_system* particle_sys);

#endif
