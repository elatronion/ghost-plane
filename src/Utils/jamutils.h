#ifndef JAM_UTILS_H
#define JAM_UTILS_H

#define SPRITE_WIDTH 16
#define SPRITE_HEIGHT 16

typedef struct {
  int x;
  int y;
} v2i;

float RandomFloat(float a, float b);

#endif
